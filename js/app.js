let app = new Vue({
    el: '#app',
    data: {
        isActiveTab: 1,
        isActiveProfileTab: 1,
        isActiveServiceTab: 1,
        isOrderShow: false,
        image: null,
        imageSrc: '',
        isShow: false,
        isOpen: false,
        addReview: false,
        isLogged: true,
        master: true,
        editServices: false,
        showEditForm: false,
        editProfileInfo: false,
        showAside: false
    },
    methods: {
        triggerUpload () {
            this.$refs.fileInput.click()
        },
        onFileChange(e) {
            const file = e.target.files[0];
            const reader = new FileReader();
            reader.onload = e => {
                this.imageSrc = reader.result
            };
            reader.readAsDataURL(file);
            this.image = file
        },
        isOpenReview(event) {
            console.log(event);
        },
        footerResize () {
            let footerHeight = document.querySelector('.main_footer').offsetHeight;
            let wrapper = document.querySelector('.contentWrapper');
            // if (window.outerWidth > 768) {
                wrapper.style.paddingBottom = footerHeight + 'px';
            // } else {
            //     wrapper.style.paddingBottom = 'auto'
            // }
        },
        watch () {
            this.isActiveProfileTab = 3;
            console.log(2);
            let container = document.querySelectorAll('.reviewsWrapper .item');
            console.log(container);

            container.forEach((item, i) => {
                let height = item.offsetHeight;
                let childHeight = item.querySelector('.item-text-wrapper').offsetHeight;
                console.log(height);
                console.log(childHeight);
                if (height < childHeight) {
                    this.isShow = true
                } else {
                    this.isShow = false
                }
            })
        },
        isShowAside () {
            this.showAside = !this.showAside;

            if (this.showAside) {
                document.querySelector('body').classList.add('body-hidden')
            } else {
                document.querySelector('body').classList.remove('body-hidden')
            }
        }
    },
    created () {
        window.addEventListener('resize', this.footerResize);
        this.footerResize();
    }
});